
[![](https://www.r-pkg.org/badges/version-ago/mcmcabn)](http://cran.rstudio.com/web/packages/mcmcabn/index.html)
[![Downloads](http://cranlogs.r-pkg.org/badges/grand-total/mcmcabn)](http://cran.rstudio.com/web/packages/mcmcabn/index.html)
[![Downloads](http://cranlogs.r-pkg.org/badges/mcmcabn)](http://cran.rstudio.com/web/packages/mcmcabn/index.html)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

___

# mcmcabn: A Structural Mcmc Sampler for Dags Learned from Observed Systemic Datasets

## Quickstart

To install `mcmabn` you need two R packages: [abn](https://CRAN.R-project.org/package=abn) and [gRbase](https://CRAN.R-project.org/package=gRbase) which requires libraries not stored on [CRAN](https://cran.r-project.org/) but on [bioconductor](http://www.bioconductor.org/). Hence you **must** install these packages **before** installing `mcmcabn`:

``` r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install(c("RBGL","Rgraphviz","graph"),  version = "3.8")

install.packages("mcmcabn", dependencies = TRUE)
```

The three main problems addressed by this R package are:

- selecting the most probable structure based on a cache of pre-computed scores.
- controlling for overfitting.
- sampling the landscape of high scoring structures.

The latter could be beneficial in an applied perspective to avoid reducing the richness of Bayesian network modeling to report only **one** structure. Indeed, it allows the user to quantify the marginal impact of relationships of interest by marginalizing out over structures or nuisance dependencies. Structural MCMC seems a very elegant and natural way to estimate the true marginal impact, so one can determine if it’s magnitude is big enough to consider as a worthwhile intervention.

## Description

Flexible implementation of a structural MCMC sampler for Directed Acyclic Graphs (DAGs). It supports the new edge reversal move from Grzegorczyk and Husmeier (2008) <doi:10.1007/s10994-008-5057-7> and the Markov blanket resampling from Su and Borsuk (2016) <http://jmlr.org/papers/v17/su16a.html>. It supports three priors: a prior controlling for structure complexity from Koivisto and Sood (2004) <http://dl.acm.org/citation.cfm?id=1005332.1005352>, an uninformative prior and a user-defined prior. The three main problems that can be addressed by this R package are selecting the most probable structure based on a cache of pre-computed scores, controlling for overfitting, and sampling the landscape of high scoring structures. It allows us to quantify the marginal impact of relationships of interest by marginalizing out over structures or nuisance dependencies. Structural MCMC seems an elegant and natural way to estimate the true marginal impact, so one can determine if it's magnitude is big enough to consider as a worthwhile intervention.

___

## What's New 

- 08/03/2019 - mcmcabn is available on CRAN (v 0.1).

- 18/02/2019 - new pre-print [Is a single unique Bayesian network enough to accurately represent your data?](https://arxiv.org/pdf/1902.06641.pdf) on arXiv

- 01/07/2019 - mcmcabn 0.2 available on CRAN.

- 02/03/2020 - mcmcabn 0.3 available on CRAN. New peer reviewed article [Bayesian Network Modeling Applied to Feline Calicivirus Infection Among Cats in Switzerland](https://www.frontiersin.org/articles/10.3389/fvets.2020.00073/full) in Front. Vet. Sci.

- 31/05/2021 - mcmcabn 0.4 available on CRAN. Update mcmcabn for interaction with abn 2.4

____

**`mcmcabn` is developed and maintained by [Gilles Kratzer](https://gilleskratzer.netlify.com/) and [Prof. Dr. Reinhard Furrer](https://user.math.uzh.ch/furrer/) from 
[Applied Statistics Group](https://www.math.uzh.ch/as/index.php?id=as) from the University of Zurich.**
