## mcmcabn 0.1:  

* mcmcabn(), query(), summary(), print() and plot() functions

## mcmcabn 0.2:

* patch to make compatible with abn 2.0

## mcmcabn 0.3:

* update mcmcabn to make it compatible with constraints imported from the cache of scores. Heating parameter to increase or decrease acceptance probability.
* new function CoupledHeatedmcmcabn() implementing parallel tempering
* new published article (preferred reference): Bayesian Network Modeling Applied to Feline Calicivirus Infection Among Cats in Switzerland in Front. Vet. Sci.

## mcmcmabn 0.4:

* update mcmcabn for interaction with abn 2.4

## mcmcabn 0.5:

* change of maintainer 

## mcmcabn 0.6:

* update to be in line with abn 3.0.0
